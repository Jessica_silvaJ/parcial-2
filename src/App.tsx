import React from 'react';
import './App.css';
import { BrowserRouter, Route } from 'react-router-dom';
import Cabecalho from './Componentes/Cabecalho';
import Inicio from './Componentes/Inicio';
import Rodape from './Componentes/Rodape';
import Formulario from './Componentes/Formulario';
import Apoio from './Componentes/Apoio';
import Contato from './Componentes/Contato';
import Dado from './Componentes/Dados';
import Tconosco from './Componentes/Tconosco';


function App() {
  return (
    <div className="App">
      <BrowserRouter>
         <Cabecalho /> 

         <Route exact path="/"  component={Inicio}/>
         <Route path="/Formulario" component={Formulario}/> 
         <Route path="/Apoio" component={Apoio}/>
         <Route path="/Dados" component={Dado}/>
         <Route path="/Tconosco" component={Tconosco}/>
         <Route path="/Contato" component={Contato}/>


         <Rodape/>
       </BrowserRouter>

    </div>
  );
}

export default App;
