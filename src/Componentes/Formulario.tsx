import React from 'react';
import { Redirect } from 'react-router';

export interface IProps {
    label: string;
  }
  export interface IState {
    nome: string;
    sobrenome: string;
    email: string;
    senha: string;
    redirect: boolean;  
  }

  class Formulario extends React.Component<IProps, IState> {
    constructor(props: IProps) {
      super(props);
      this.state = {
        nome: '',
        sobrenome: '',
        email: '',
        senha: '',
        redirect: false, 
        
        
      };
      this.handleChange = this.handleChange.bind(this);
      this.alterarS = this.alterarS.bind(this);
      this.alterarE = this.alterarE.bind(this);
      this.alterarSn = this.alterarSn.bind(this);
      
     
      
    }
    private handleChange(event: React.FormEvent<HTMLInputElement>): void {
      this.setState({ nome: event.currentTarget.value });
    }

    private alterarS(event: React.FormEvent<HTMLInputElement>): void {
        this.setState({ sobrenome: event.currentTarget.value });
      }

    private alterarE(event: React.FormEvent<HTMLInputElement>): void {
        this.setState({ email: event.currentTarget.value });
      }
     

      private alterarSn(event: React.FormEvent<HTMLInputElement>): void {
        this.setState({ senha: event.currentTarget.value });
      }

    
   
      chamaForm= () => {
        this.setState({
          redirect: true
        })
       }
        

    public render(): JSX.Element {

      if(this.state.redirect) {
        return <Redirect to="/Dados" />

      }
      else {

      return (
        <div className="text-center container-fluid bg-grey">

         <h2>PREENCHA O FORMULÁRIO DE INSCRIÇÃO</h2>

            <form  action='/Dados' method='POST' >


            <div className='Nome'>
        
          <span>{this.props.label}</span>
          <label>
          <h4>Nome: </h4>
          <input type='text'  className='form-control' id='nome' name='nome' placeholder=' Digite o seu nome!' required value={this.state.nome} onChange={this.handleChange} />
          </label>

          </div>

          <div className='sobrenome'>
         <span>{this.props.label}</span>
         <label>
         <h4>Sobrenome: </h4>
         <input type='text'  className='form-control' name='sobrenome' placeholder=' Digite o seu sobrenome!' required value={this.state.sobrenome} onChange={this.alterarS} />
        </label>
          </div>

          <div className='email'>
            <label>
            <h4> E-mail </h4>
              <input type='email'  className='form-control' name='email' placeholder= 'Por exemplo mauro@email.com' required value={this.state.email} onChange={this.alterarE}/>
            </label>
          </div>
               
            <div className='senha'>
           <label>
           <h4>Senha: </h4>
                <input type='password' className='form-control' name='senha' placeholder=' Digite a sua senha' required value={this.state.senha} onChange={this.alterarSn}/>&nbsp;
             </label>
              </div>

            
            <div className= 'enviar'>
              <br/>
            <input type='submit'onClick={() => this.chamaForm()} value='Enviar Formulário'/>
            </div>

          </form>
        </div>


      );
      }
    }
  }

export default Formulario;