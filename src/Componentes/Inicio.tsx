import React from 'react';
import Sobrenos from "../img/Sobre-nós.png";
import NossoV from '../img/Nossos-Valores.png';

class Inicio extends React.Component{

    public render () :  JSX.Element {
        return(
         
            <div>
           
         <div id='about' className='container-fluid'>
         <div className='row'>
         <div className='col-sm-8'>
         <h2>Sobre nós</h2><br />
         <h4>Somos uma página web experimental com intuito de desenvolver um projeto para a criação de um website de cadastro de clientes para nossos parceiros.</h4><br />
         <h4><p>Inovação e qualidade é o nosso lema, conte conosco!</p></h4><br />
         
         </div>
          <div className='col-sm-4'>
              <img src={Sobrenos} alt='logo'/>             
          </div>
          </div>
          </div>
        

          <div className='container-fluid bg-grey'>
          <div className='row'>
          <div className='col-sm-1'>

            

          </div>
          <div className='col-sm-7'>
          <h2>Nossos Valores</h2><br />
         <h4><strong>MISSÃO:</strong> Nossa missão é ser uma empresa de utilidade para as pessoas, utilizando a tecnologia para fazer um mundo melhor. Nosso foco é a inovação e a inclusão de pesssoas em todos os âmbitos, seja ele social ou empresarial.</h4><br />
         <h4><strong>VISÃO:</strong> Nossa visão de futuro é tornar-se um dos maiores desenvolvedores de softwares de utilidades para nosso parceiros, trazendo inovação e acessibilidade para todos.</h4>
          <img src={NossoV} alt='logo'/> 
           </div>
          </div>
         </div>

          </div>

        

        )

    }
}
export default Inicio;