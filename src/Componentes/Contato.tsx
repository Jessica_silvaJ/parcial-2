import React from 'react';
import Map from '../img/map.png';


class Contato extends React.Component{

    public render () :  JSX.Element {
        return(

            <div>
              
          
            
             <div id='contact' className='container-fluid'>
              <h4 className='text-center'>Nós somos responsáveis por dezenas de sites pelo o mundo cheios de pessoas que trabalham para a sua experiência
                no Form React Project ser cada vez melhor</h4>
              <div className='row'>
                <div className='col-sm-5 -bg-grey'>
                <img src={Map} alt='logo'/>
                  <p>Entre em contato conosco:</p>
                  <p><span className='glyphicon glyphicon-map-marker'></span> Salvador, BA</p>
                  <p><span className='glyphicon glyphicon-phone'></span> +55 (71) 96325698</p>
                  <p><span className='glyphicon glyphicon-envelope'></span> formreactproject@website.com</p>
                </div>
               
                    
                   
                        </div>

                   
                  </div>
             </div> 

        )

    }
}
export default Contato;