import React from 'react';

class Rodape extends React.Component{

    public render () :  JSX.Element {
        return(
            
            <footer className='container-fluid text-center'>
            <a href='#mypage' title='To Top'>
              <span className='glyphicon glyphicon-chevron-up'></span>
            </a>
            <p>Created by Roberto Silva & Jéssica Silva</p>
           </footer>
          
          
        )

    }
}
export default Rodape;